from predictors.DarknetPredictor import DarknetPredictor


from pathlib import Path


class YoloRPredictor(DarknetPredictor):
    CONFIDENCE_THRESHOLD: float = 0.25
    IOU_THRESHOLD: float = 0.5

    def __init__(
        self,
        weights_path: Path,
        image_size: int,
        use_cpu: bool = False,
        config_name: str = "yolor_csp",
        swap_bgr: bool = True,
        confidence_threshold: float = CONFIDENCE_THRESHOLD,
        iou_threshold: float = IOU_THRESHOLD,
    ):
        super().__init__(
            weights_path=weights_path,
            image_size=image_size,
            use_cpu=use_cpu,
            config_name=config_name,
            swap_bgr=swap_bgr,
            confidence_threshold=confidence_threshold,
            iou_threshold=iou_threshold,
        )
