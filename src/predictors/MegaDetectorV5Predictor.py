from predictors.base import AIPredictor
from predictors.utils import (
    letterbox,
    non_max_suppression,
    scale_coords,
    truncate_float,
    truncate_float_array,
    yolo2xyxy,
)


import numpy as np
import torch
import yolov5

import logging

log = logging.getLogger(__name__)


class MegaDetectorV5Predictor(AIPredictor):
    MODEL_FILE_EXTENSION = ".pt"
    STRIDE = 64
    BATCH_SIZE = 4
    CONF_DIGITS = 3
    COORD_DIGITS = 4
    IMAGE_SIZE = 1280
    # It is smaller than in V4.0 as it was
    # recommended in MD docs
    PROBABILITY_THRESHOLD = 0.7

    def __init__(
        self,
        checkpoint_path: str,
        use_cpu: bool = True,
        probability_threshold: float = PROBABILITY_THRESHOLD,
        image_size: int = IMAGE_SIZE,
        stride: int = STRIDE,
        conf_digits: int = CONF_DIGITS,
        coord_digits: int = COORD_DIGITS,
        swap_bgr: bool = False,
    ):
        self.probability_threshold = probability_threshold
        self.image_size = image_size
        self.stride = stride
        self.conf_digits = conf_digits
        self.coord_digits = coord_digits

        self.device = (
            torch.device("cuda:0")
            if torch.cuda.is_available() and not use_cpu
            else "cpu"
        )

        self.model = yolov5.load(checkpoint_path)
        # self.model = checkpoint['model'].float().fuse().eval()  # FP32 model

        if (self.device != "cpu") and torch.cuda.is_available():
            log.info("Sending model to GPU")
            self.model.to(self.device)

        self.swap_bgr = swap_bgr

    def _preprocess_input_image(self, img_original):
        # padded resize
        # JIT requires auto=False
        img = letterbox(
            img_original, new_shape=self.image_size, stride=self.stride, auto=True
        )[0]

        if self.swap_bgr:
            img = img[:, :, ::-1]

        img = img.transpose((2, 0, 1))
        img = np.ascontiguousarray(img)
        img = torch.from_numpy(img)
        img = img.to(self.device)
        img = img.float()
        img /= 255

        if (
            len(img.shape) == 3
        ):  # always true for now, TODO add inference using larger batch size
            img = torch.unsqueeze(img, 0)

        return img

    def predict(self, image_matrix: np.array):
        bboxes = []
        scores = []
        classes = []

        img = self._preprocess_input_image(image_matrix)

        # NMS
        log.info("Using probability threshold of %s", self.probability_threshold)
        pred = non_max_suppression(
            prediction=self.model(img), conf_thres=self.probability_threshold
        )

        # format detections/bounding boxes
        gn = torch.tensor(image_matrix.shape)[[0, 1, 0, 1]]  # normalization gain hwhw

        for det in pred:
            if not len(det):
                continue
            # Rescale boxes from img_size to im0 size
            det[:, :4] = scale_coords(
                img.shape[2:], det[:, :4], image_matrix.shape
            ).round()

            for *xyxy, conf, cls in reversed(det):
                xyxy = (yolo2xyxy(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()
                conf = truncate_float(conf.tolist(), precision=self.conf_digits)

                # MegaDetector output format's categories start at 1, but this model's start at 0
                cls = int(cls.tolist()) + 1
                if cls not in (1, 2, 3):
                    raise KeyError(f"{cls} is not a valid class.")

                bboxes.append(truncate_float_array(xyxy, precision=self.coord_digits))
                scores.append(conf)
                classes.append(float(cls))

        return {"bboxes": bboxes, "scores": scores, "classes": classes}
