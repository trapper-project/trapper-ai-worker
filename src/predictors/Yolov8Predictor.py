from predictors.base import AIPredictor
from predictors.utils import (
    trapper2yolo_bbox,
    yolo2pixels_bbox,
    crop_square_cv_to_pil
)

import numpy as np
from ultralytics import YOLO

from pathlib import Path
from typing import List, Tuple, Optional


class Yolov8Predictor(AIPredictor):
    MODEL_FILE_EXTENSION = ".pt"
    CONFIDENCE_THRESHOLD: float = 0.25
    IOU_THRESHOLD: float = 0.7
    AGNOSTIC_NMS: bool = True

    def __init__(self, weights_path: Path, image_size: int):
        self.model = YOLO(model=weights_path)
        self.image_size = image_size

    def _predict(
        self,
        image_matrix: np.array,
        all_boxes: List[Tuple[float, float, float, float]],
        all_scores: List[float],
        all_classes: List[float],
        crop=False
    ) -> Tuple[List, List, List]:
        """
        Private function template for YOLO models execution
        """

        for result in self.model(
            image_matrix,
            imgsz=self.image_size,
            conf=self.CONFIDENCE_THRESHOLD,
            iou=self.IOU_THRESHOLD,
            agnostic_nms=self.AGNOSTIC_NMS,
        ):
            predictions = result.boxes.data.cpu().numpy()

            if not crop:
                boxes = predictions[:, :4].copy()

                boxes[:, 0] = predictions[:, 1] / image_matrix.shape[0]
                boxes[:, 1] = predictions[:, 0] / image_matrix.shape[1]
                boxes[:, 2] = predictions[:, 3] / image_matrix.shape[0]
                boxes[:, 3] = predictions[:, 2] / image_matrix.shape[1]

                all_boxes.extend(boxes.tolist())

            all_scores.extend(predictions[:, 4].tolist())
            all_classes.extend(predictions[:, 5].tolist())

        return all_boxes, all_scores, all_classes

    def predict(
        self,
        image_matrix: np.array,
        detected_objects: Optional[List[dict]] = None
    ) -> dict:
        """
        YOLOv8-based model predict animal species classes
        for native image or for each individual crop
        """
        all_boxes = []
        all_scores = []
        all_classes = []

        # Make prediction with native (full) image
        if not detected_objects:
            all_boxes, all_scores, all_classes = self._predict(
                image_matrix=image_matrix,
                all_boxes=all_boxes,
                all_scores=all_scores,
                all_classes=all_classes,
                crop=False
            )
        # Make prediction for each individual crop delivered
        # by object detection model
        else:
            for sample in detected_objects:
                # Empty bboxes is possible when human or vehicle was detected
                # by detection step or the image is empty
                trapper_bbox = sample.get("bboxes", None)
                if trapper_bbox:
                    trapper_bbox = trapper_bbox[0]

                    # Convert Trapper bbox to YOLO bbox format
                    yolo_bbox = trapper2yolo_bbox(trapper_bbox)

                    # Convert YOLO bbox format to pixels
                    pixels_bbox = yolo2pixels_bbox(image_matrix, yolo_bbox)

                    # Convert pixels to Trapper bbox
                    w0 = pixels_bbox[1] / image_matrix.shape[0]
                    w1 = pixels_bbox[0] / image_matrix.shape[1]
                    w2 = pixels_bbox[3] / image_matrix.shape[0]
                    w4 = pixels_bbox[2] / image_matrix.shape[1]
                    new_trapper_bbox = [w0, w1, w2, w4]
                    all_boxes.append(new_trapper_bbox)

                    # Crop image as square based on pixels
                    cropped_image = crop_square_cv_to_pil(image_matrix, pixels_bbox)

                    all_boxes, all_scores, all_classes = self._predict(
                        image_matrix=cropped_image,
                        all_boxes=all_boxes,
                        all_scores=all_scores,
                        all_classes=all_classes,
                        crop=True
                    )

        return {"bboxes": all_boxes, "scores": all_scores, "classes": all_classes}
