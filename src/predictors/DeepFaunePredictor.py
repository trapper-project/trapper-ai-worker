from predictors.base import AIPredictor
from predictors.utils import (
    trapper2yolo_bbox,
    yolo2pixels_bbox,
    crop_square_cv_to_pil
)

import numpy as np
from ultralytics import YOLO

from pathlib import Path
from typing import List, Optional

import timm
import torch
import torch.nn as nn
from torch import tensor
from torchvision.transforms import InterpolationMode, transforms
from PIL import Image


class DeepFauneDetectionPredictor(AIPredictor):
    """
    DeepFaune object detection v1.1

    This class is implementing Detector class from official DF repository
    https://plmlab.math.cnrs.fr/deepfaune/software/-/blob/master/detectTools.py?#L48-86
    """

    MODEL_FILE_EXTENSION = ".pt"
    CONFIDENCE_THRESHOLD: float = 0.6
    IOU_THRESHOLD: float = 0.7
    AGNOSTIC_NMS: bool = True

    def __init__(self, weights_path: Path, image_size: int):
        self.model = YOLO(model=weights_path)
        self.image_size = image_size

    def predict(self, image_matrix: np.array) -> dict:
        """
        YOLO-based DeepFaune object detection model predict objects classes
        """

        all_boxes = []
        all_scores = []
        all_classes = []

        for result in self.model(
            image_matrix,
            imgsz=self.image_size,
            conf=self.CONFIDENCE_THRESHOLD,
            iou=self.IOU_THRESHOLD,
            agnostic_nms=self.AGNOSTIC_NMS,
        ):
            predictions = result.boxes.data.cpu().numpy()
            boxes = predictions[:, :4].copy()

            boxes[:, 0] = predictions[:, 1] / image_matrix.shape[0]
            boxes[:, 1] = predictions[:, 0] / image_matrix.shape[1]
            boxes[:, 2] = predictions[:, 3] / image_matrix.shape[0]
            boxes[:, 3] = predictions[:, 2] / image_matrix.shape[1]

            all_boxes.extend(boxes.tolist())
            all_scores.extend(predictions[:, 4].tolist())
            all_classes.extend(predictions[:, 5].tolist())

        return {"bboxes": all_boxes, "scores": all_scores, "classes": all_classes}


class DeepFauneBackboneModel(nn.Module):
    """
    DeepFaune backbone model

    This class is implementing Model class from official DF repository
    https://plmlab.math.cnrs.fr/deepfaune/software/-/blob/master/classifTools.py#L87-142
    """

    BACKBONE = "vit_large_patch14_dinov2.lvd142m"

    def __init__(self, num_classes: int):
        """
        Constructor of classifier model
        """
        super().__init__()

        # Number of unique animal species classes
        # In DF the value is represented by "txt_animalclasses" dict
        self.num_classes = num_classes

        self.base_model = timm.create_model(
            self.BACKBONE,
            pretrained=False,
            num_classes=self.num_classes,
            dynamic_img_size=True
        )
        self.backbone = self.BACKBONE

        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    def forward(self, input):
        x = self.base_model(input)
        return x

    def predict(self, data, with_softmax=True):
        """
        Predict on test DataLoader
        :param test_loader: test dataloader: torch.utils.data.DataLoader
        :return: numpy array of predictions without soft max
        """
        self.eval()

        self.to(self.device)

        total_output = []

        with torch.no_grad():
            x = data.to(self.device)

            if with_softmax:
                output = self.forward(x).softmax(dim=1)
            else:
                output = self.forward(x)

            total_output += output.tolist()

        return np.array(total_output)

    def load_weights(self, path):
        """
        :param path: path of .pt save of model
        """
        try:
            params = torch.load(path, map_location=self.device)

            args = params["args"]
            num_classes = args.get("num_classes", 0)

            if self.num_classes != num_classes:
                raise Exception(
                    f"[DeepFaune] You load a model ({num_classes})"
                    f" that does not have the same number of class ({self.num_classes})"
                )
            self.backbone = args["backbone"]
            self.load_state_dict(params["state_dict"])
        except Exception as e:
            print(f"[DeepFaune] Can't load checkpoint model because: {str(e)}")
            raise e


class DeepFauneClassificationPredictor(AIPredictor):
    """
    DeepFaune species classifier v1.2

    This class is implementing Classifier class from official DF repository
    https://plmlab.math.cnrs.fr/deepfaune/software/-/blob/master/classifTools.py#L64-80
    """

    def __init__(self, weights_path, num_classes: int, crop_size: int):
        self.model = DeepFauneBackboneModel(
            num_classes=num_classes
        )
        self.model.load_weights(weights_path)

        self.transforms = transforms.Compose([
            transforms.Resize(
                size=(crop_size, crop_size),
                interpolation=InterpolationMode.BICUBIC,
                max_size=None,
                antialias=None
            ),
            transforms.ToTensor(),
            transforms.Normalize(
                mean=tensor([0.4850, 0.4560, 0.4060]),
                std=tensor([0.2290, 0.2240, 0.2250])
            )
        ])

    def predict_on_batch(self, batch_tensor, with_softmax=True):
        return self.model.predict(batch_tensor, with_softmax)

    def preprocess_image(self, cropped_image):
        preprocess_image = self.transforms(cropped_image)

        return preprocess_image.unsqueeze(dim=0)

    def predict(self, image_matrix: np.array, detected_objects: Optional[List[dict]] = None) -> dict:
        """
        DeepFaune species model predict animal class for each individual crop
        """

        all_boxes = []
        all_scores = []
        all_classes = []

        if detected_objects:
            for sample in detected_objects:
                # Empty bboxes is possible when human or vehicle was detected
                # by detection step or the image is empty
                trapper_bbox = sample.get("bboxes", None)
                if trapper_bbox:
                    trapper_bbox = trapper_bbox[0]

                    # Convert Trapper bbox to YOLO bbox format
                    yolo_bbox = trapper2yolo_bbox(trapper_bbox)

                    # Convert YOLO bbox format to pixels
                    pixels_bbox = yolo2pixels_bbox(image_matrix, yolo_bbox)

                    # Convert pixels to Trapper bbox
                    w0 = pixels_bbox[1] / image_matrix.shape[0]
                    w1 = pixels_bbox[0] / image_matrix.shape[1]
                    w2 = pixels_bbox[3] / image_matrix.shape[0]
                    w4 = pixels_bbox[2] / image_matrix.shape[1]
                    new_trapper_bbox = [w0, w1, w2, w4]

                    # Crop image as square based on pixels
                    cropped_image = crop_square_cv_to_pil(image_matrix, pixels_bbox)

                    batch_tensor = self.preprocess_image(cropped_image)
                    scores = self.predict_on_batch(batch_tensor)
                    species_max_confidence = np.argmax(scores[0, :])

                    all_boxes.append(new_trapper_bbox)
                    all_scores.append(float(scores[0][species_max_confidence]))
                    all_classes.append(species_max_confidence)
        else:
            raise NotImplementedError("[DeepFaune] Classifier expect to work with bboxes")

        return {"bboxes": all_boxes, "scores": all_scores, "classes": all_classes}
