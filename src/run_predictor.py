import argparse
import gc
import json

import cv2
import numpy as np
from predictors.predictor_factory import AIPredictorFactory


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--predictor_class", type=str, required=True)
    parser.add_argument("--model_file_path", type=str, required=True)
    parser.add_argument("--model_config", type=str, default="{}")
    parser.add_argument("--image_path", type=str, required=True)
    parser.add_argument("--repeat", type=int, default=1)
    args = parser.parse_args()

    predictor_factory = AIPredictorFactory()

    predictor = predictor_factory.create_predictor(
        predictor_class=args.predictor_class,
        model_file_path=args.model_file_path,
        model_config=json.loads(args.model_config),
    )

    with open(args.image_path, "rb") as f:
        image_bytes = f.read()

        img = cv2.imdecode(np.frombuffer(image_bytes, np.uint8), cv2.IMREAD_COLOR)

    for _ in range(args.repeat):
        results = predictor.predict(img)
        print(results)
        gc.collect()


if __name__ == "__main__":
    main()
