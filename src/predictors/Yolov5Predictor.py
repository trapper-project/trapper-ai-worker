from predictors.base import AIPredictor


import numpy as np
import yolov5


from pathlib import Path


class Yolov5Predictor(AIPredictor):
    MODEL_FILE_EXTENSION = ".pt"
    CONFIDENCE_THRESHOLD: float = 0.25
    IOU_THRESHOLD: float = 0.6
    CLASS_AGNOSTIC: bool = False
    MULTI_LABEL: bool = False
    MAX_DET: int = 1000

    def __init__(self, weights_path: Path, image_size: int):
        self.model = yolov5.load(str(weights_path))

        self.model.conf = self.CONFIDENCE_THRESHOLD  # NMS confidence threshold
        self.model.iou = self.IOU_THRESHOLD  # NMS IoU threshold
        self.model.agnostic = self.CLASS_AGNOSTIC  # NMS class-agnostic
        self.model.multi_label = self.MULTI_LABEL  # NMS multiple labels per box
        self.model.max_det = self.MAX_DET  # maximum number of detections per image

        self.image_size = image_size

    def predict(self, image_matrix: np.array) -> dict:
        results = self.model(image_matrix, size=self.image_size)
        predictions = results.pred[0].cpu()
        boxes = predictions[:, :4].detach().clone()

        boxes[:, 0] = predictions[:, 1] / image_matrix.shape[0]
        boxes[:, 1] = predictions[:, 0] / image_matrix.shape[1]
        boxes[:, 2] = predictions[:, 3] / image_matrix.shape[0]
        boxes[:, 3] = predictions[:, 2] / image_matrix.shape[1]

        boxes = boxes.tolist()

        scores = predictions[:, 4].tolist()
        classes = predictions[:, 5].tolist()

        return {"bboxes": boxes, "scores": scores, "classes": classes}
