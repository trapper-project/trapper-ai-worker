import os

TRAPPERAI_USERNAME = os.environ["TRAPPERAI_USERNAME"]
TRAPPERAI_PASSWORD = os.environ["TRAPPERAI_PASSWORD"]
TRAPPERAI_RABBIT_URL = os.environ["TRAPPERAI_RABBIT_URL"]
broker_url = f"amqp://{TRAPPERAI_USERNAME}:{TRAPPERAI_PASSWORD}@{TRAPPERAI_RABBIT_URL}"

# fetch only one batch at a time
worker_prefetch_multiplier = 1

# acknowledge tasks ofter finishing processing, not after starting
# this should make jobs more resilient during worker crashes
task_acks_late = True
