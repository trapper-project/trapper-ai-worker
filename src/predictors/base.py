import numpy as np
from abc import abstractmethod
from typing import List, Optional


class AIPredictor:
    @abstractmethod
    def predict(self, image_matrix: np.array) -> dict:
        pass
