### 08.01.2025

* Added support for MegaDetector V6 model:
  * animal, people and vehicule detection via [MegaDetectroV6-Ultralytics-YoloV9-Compact (yolov9c)](https://github.com/microsoft/CameraTraps/blob/main/PytorchWildlife/models/detection/ultralytics_based/megadetectorv6.py#L34);
* Upgraded venv (ultralytics);


### 23.12.2024

* Added support for DeepFaune AI models:
  * animal, people and vehicule detection via [deepfaune-yolov8s_960.pt](https://pbil.univ-lyon1.fr/software/download/deepfaune/v1.1/);
  * animal species classification via [deepfaune-vit_large_patch14_dinov2.lvd142m.v2.pt](https://pbil.univ-lyon1.fr/software/download/deepfaune/v1.2/;);
* Added support for optional image cropping before species model execution;
* Upgraded `Dockerfile` (image from `nvidia/cuda` to `pytorch/pytorch`) and `docker-compose.gpu`;
* Upgraded venv (ultralytics, timm, torch, dill, yolov5);