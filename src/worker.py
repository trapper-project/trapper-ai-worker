import enum
import hashlib
import json
import os
from pathlib import Path
from typing import Dict, Generic, List, Optional, TypeVar
import cv2
import numpy as np
from pydantic import BaseModel
from pydantic.generics import GenericModel

import structlog
from celery import Celery, Task
from celery import signals

import requests

from urllib.parse import urljoin

from tenacity import retry, stop_after_attempt, wait_fixed, RetryError
from predictors.predictor_factory import AIPredictorFactory

from predictors.base import AIPredictor
from celery.utils.log import get_task_logger


QUEUE_NAME_PREFIX = "trapper-ai-runtime"

app = Celery(
    "TrapperAI Runtime Worker",
)

os.environ["CELERY_CONFIG_MODULE"] = "celeryconfig"


logger: structlog.BoundLogger = structlog.wrap_logger(get_task_logger(__name__))


@signals.task_prerun.connect
def on_task_prerun(sender, task_id, task, args, kwargs, **_):
    structlog.contextvars.bind_contextvars(task_id=task_id, task_name=task.name)


class ModelInfo(BaseModel):
    id: str
    model_file_hash: str
    predictor_class: str
    model_config: dict


# Those values must match exactly values specified in TrapperAI
class ProcessingStage(enum.Enum):
    CREATED = 100
    RESOURCE_DOWNLOADING = 200
    RESOURCE_DOWNLOADING_FINISHED = 250
    PREDICTION = 300
    FINISHED = 400


class ProcesingStatus(enum.Enum):
    CREATED = 100
    IN_PROGRESS = 200
    SUCCEEDED = 300
    FAILED = 400
    CANCELED = 500


class ResourceProcessingStage(enum.Enum):
    INITIAL = 0
    IMAGE_CRAWLING = 100
    IMAGE_PREPROCESSING = 200
    MODEL_PREDICTION = 300
    FINISHED = 400


class ResourceProcessingStatus(enum.Enum):
    INITIAL = 0
    IN_PROGRESS = 100
    FINISHED = 200
    FAILED = 300


StatusT = TypeVar("StatusT")


class StatusDto(GenericModel, Generic[StatusT]):
    label: str
    value: StatusT


class ResourceInfo(BaseModel):
    id: str
    detected_objects: str
    status: StatusDto[ResourceProcessingStatus]
    stage: StatusDto[ResourceProcessingStage]


class MinimalPredictionJob(BaseModel):
    id: str
    prediction_model_id: str


class ResourceBatch(BaseModel):
    id: str
    resources: List[ResourceInfo]
    processing_stage: StatusDto[ProcessingStage]
    processing_status: StatusDto[ProcesingStatus]
    job: MinimalPredictionJob


class Predictions(BaseModel):
    bboxes: List[List[float]]
    scores: List[float]
    classes: List[float]


class ResourceProcessingResult(BaseModel):
    id: str
    errors: Optional[List[str]]
    predictions: Optional[Predictions]


class ResourceProcessingResultList(BaseModel):
    __root__: List[ResourceProcessingResult]


class TrapperAIClient:
    _access_token: Optional[str]
    _refresh_token: Optional[str]

    def __init__(self) -> None:
        self.trapperai_url = os.environ["TRAPPERAI_URL"]
        self.trapperai_username = os.environ["TRAPPERAI_USERNAME"]
        self.trapperai_password = os.environ["TRAPPERAI_PASSWORD"]
        self._access_token = None
        self._refresh_token = None

    def obtain_new_token(self):
        response = requests.post(
            urljoin(self.trapperai_url, "/api/token/"),
            json={
                "username": self.trapperai_username,
                "password": self.trapperai_password,
            },
        )

        response.raise_for_status()

        self._access_token = response.json()["access"]
        self._refresh_token = response.json()["refresh"]

    def refresh_token(self):
        if not self._refresh_token:
            self.obtain_new_token()
            return
        try:
            response = requests.post(
                urljoin(self.trapperai_url, "/api/token/refresh/"),
                json={
                    "refresh": self._refresh_token,
                },
            )

            if response.status_code == 400:
                logger.error("refresh token request failed", r=response.content)

            response.raise_for_status()

            logger.info("refresh", r=response.json())

            self._access_token = response.json()["access"]
            # self._refresh_token = response.json()["refresh"]
        except requests.RequestException:
            self.obtain_new_token()

    @retry(stop=stop_after_attempt(5), wait=wait_fixed(1))
    def _get(
        self,
        endpoint: str,
        is_json=True,
        timeout: Optional[int] = None,
        streaming=False,
    ):
        response = requests.get(
            urljoin(self.trapperai_url, endpoint),
            headers={"Authorization": f"Bearer {self._access_token}"},
            timeout=timeout,
            stream=streaming,
        )

        if not response.ok:
            logger.error(
                "Error during communication with TrapperAI server",
                endpoint=endpoint,
                status_code=response.status_code,
                error=response.content,
            )
            if response.status_code == 401:
                logger.info("Request failed with 401, trying to refresh token")
                self.refresh_token()
                return self._get(endpoint, is_json, timeout)

        response.raise_for_status()

        if streaming:
            return response

        if is_json:
            return response.json()
        return response.content

    def _post(self, endpoint: str, data: dict):
        response = requests.post(
            urljoin(self.trapperai_url, endpoint),
            json=data,
            headers={"Authorization": f"Bearer {self._access_token}"},
        )

        if response.status_code == 400:
            logger.error(
                "Error during communication with TrapperAI server",
                error=response.json(),
            )

        response.raise_for_status()

        return response.json()

    def _patch(self, endpoint: str, data: dict):
        response = requests.patch(
            urljoin(self.trapperai_url, endpoint),
            json=data,
            headers={"Authorization": f"Bearer {self._access_token}"},
        )

        if response.status_code == 400:
            logger.error(
                "Error during communication with TrapperAI server",
                error=response.json(),
            )

        response.raise_for_status()

        return response.json()

    def fetch_batch_data(self, batch_id: str):
        data = self._get(f"/api/resource_batches/{batch_id}/")
        return ResourceBatch.parse_obj(data)

    def fetch_image(self, resource_id: str):
        data = self._get(
            f"/api/resources/{resource_id}/image/", is_json=False, timeout=30
        )

        return cv2.imdecode(np.fromstring(data, np.uint8), cv2.IMREAD_COLOR)

    def fetch_model_info(self, model_id: str):
        data = self._get(f"/api/prediction_models/{model_id}/")
        return ModelInfo.parse_obj(data)

    def fetch_model_file(self, model_id: str):
        return self._get(
            f"/api/prediction_models/{model_id}/model_file/",
            is_json=False,
            timeout=600,
            streaming=True,
        )

    def save_batch_results(
        self, batch_id: str, results: List[ResourceProcessingResult]
    ):
        logger.info("save_batch_results", results=results)
        return self._post(
            f"/api/resource_batches/{batch_id}/save_results/",
            data=ResourceProcessingResultList(__root__=results).dict()["__root__"],
        )

    def update_batch_processing_stage(self, batch_id: str, new_stage: ProcessingStage):
        self._patch(
            f"/api/resource_batches/{batch_id}/", {"processing_stage": new_stage.value}
        )

    def update_batch_processing_status(
        self, batch_id: str, new_status: ProcesingStatus, error_msg: str = ""
    ):
        self._patch(
            f"/api/resource_batches/{batch_id}/",
            {"processing_status": new_status.value, "processing_error_msg": error_msg},
        )


class PredictionModelManager:
    loaded_models: Dict[str, AIPredictor]
    loaded_models_config: Dict[str, dict]
    loaded_models_file_hash: Dict[str, str]

    def __init__(self) -> None:
        self.loaded_models = dict()
        self.loaded_models_config = dict()
        self.loaded_models_file_hash = dict()
        self.model_cache_dir = Path("/model_cache")
        self.model_cache_dir.mkdir(exist_ok=True)
        self.factory = AIPredictorFactory()

    def _calculate_checksum(self, file_path: Path) -> str:
        with file_path.open("rb") as f:
            h = hashlib.sha256()
            for chunk in iter(lambda: f.read(4096), b""):
                h.update(chunk)
            return h.hexdigest()

    def _get_model_file_name(self, model_info: ModelInfo):
        ext = self.factory.get_model_file_extension()
        return f"{model_info.id}{ext}"

    def get_predictor(self, client: TrapperAIClient, model_info: ModelInfo):
        import gc

        gc.collect()
        if model_info.id in self.loaded_models:
            if json.dumps(model_info.model_config, sort_keys=True) != json.dumps(
                self.loaded_models_config[model_info.id], sort_keys=True
            ):
                logger.info(
                    "Predictor already loaded, but with different config",
                    model_info=model_info,
                )
            elif (
                model_info.model_file_hash
                != self.loaded_models_file_hash[model_info.id]
            ):
                logger.info(
                    "Predictor already loaded, but with different model file",
                    model_info=model_info,
                )
            else:
                logger.info("Predictor already loaded", model_info=model_info)
                return self.loaded_models[model_info.id]

        model_file_path = self.model_cache_dir / self._get_model_file_name(model_info)

        if (
            not model_file_path.exists()
            or self._calculate_checksum(model_file_path) != model_info.model_file_hash
        ):
            logger.info("Downloading model", model_info=model_info)
            with model_file_path.open("wb") as f:
                response = client.fetch_model_file(model_info.id)
                for chunk in response.iter_content(chunk_size=4096):
                    f.write(chunk)

            logger.info("Verifying model file checksum", model_info=model_info)
            downloaded_file_checksum = self._calculate_checksum(model_file_path)
            if downloaded_file_checksum != model_info.model_file_hash:
                logger.error(
                    "Model file checksum does not match",
                    model_info=model_info,
                    downloaded_file_checksum=downloaded_file_checksum,
                )
                raise RuntimeError("Model file checksum does not match")

            logger.info("Model downloaded", model_info=model_info)

        logger.info("Loading model from file", model_info=model_info)
        predictor = self.factory.create_predictor(
            predictor_class=model_info.predictor_class,
            model_file_path=model_file_path,
            model_config=model_info.model_config,
        )
        logger.info("Model loaded", model_info=model_info)

        self.loaded_models[model_info.id] = predictor
        self.loaded_models_config[model_info.id] = model_info.model_config
        self.loaded_models_file_hash[model_info.id] = model_info.model_file_hash

        return predictor


prediction_model_manager = PredictionModelManager()


class RunBatchPrediction(Task):
    name = "trapperai_runtime.run_batch_prediction"
    acks_late = True
    autoretry_for = (Exception,)
    retry_backoff = 5
    retry_jitter = True
    max_retries = 3
    track_started = True

    def __init__(self) -> None:
        super().__init__()

        self.client = TrapperAIClient()

    def process_batch(self, batch_id: str):
        log = logger.bind(batch_id=batch_id)

        log.info("Started processing of resource batch")

        log.info("Obtaining auth token")
        client = self.client
        client.refresh_token()

        client.update_batch_processing_stage(batch_id, ProcessingStage.PREDICTION)

        log.info("Fetching resource batch data")
        batch = client.fetch_batch_data(batch_id)

        if batch.processing_status.value == ProcesingStatus.CANCELED:
            log.info("This batch was cancelled, skipping!")

        log.info("Fetching prediction model info")
        model_info = client.fetch_model_info(batch.job.prediction_model_id)

        log.info("Preparing predictor")
        predictor = prediction_model_manager.get_predictor(client, model_info)

        results = []

        for i, resource in enumerate(batch.resources):
            log.info(
                "Processing resource",
                resource_no=i + 1,
                batch_size=len(batch.resources),
            )

            if resource.status.value != ResourceProcessingStatus.IN_PROGRESS:
                log.warning("Invalid resource state, skipping", resource_id=resource.id)

            log.info("Fetching resource image", resource_id=resource.id)
            image = client.fetch_image(resource.id)
            log.info("Resource image fetched", resource_id=resource.id)

            log.info("Running prediction", resource_id=resource.id)

            log.info(f"[DEBUG] RunBatchPrediction detected_objects: {resource.detected_objects}")
            try:
                detected_objects = json.loads(resource.detected_objects)
                # Species classification with bboxes (crop-based approach)
                if detected_objects:
                    bboxes = predictor.predict(
                        image,
                        detected_objects=detected_objects
                    )
                    log.info(f"[DEBUG] RunBatchPrediction Crop-based approach")
                # Object detection or Species classification without bboxes (native image)
                else:
                    bboxes = predictor.predict(image)
                    log.info(f"[DEBUG] RunBatchPrediction Native approach")
            except json.JSONDecodeError:
                # In the case when detected objects can not be parsed from JSON use
                # Object detection or Species classification without bboxes (native image)
                bboxes = predictor.predict(image)
                log.info(f"[DEBUG] RunBatchPrediction Native approach after exception")

            result = ResourceProcessingResult(
                id=resource.id, predictions=Predictions.parse_obj(bboxes)
            )
            results.append(result)

            log.info(
                "Prediction for resource finished",
                resource_no=i + 1,
                batch_size=len(batch.resources),
                result=result,
            )

        log.info("Saving processing results")
        client.save_batch_results(batch.id, results)
        client.update_batch_processing_stage(batch_id, ProcessingStage.FINISHED)

        log.info("Batch processing finished")

    def run(self, batch_id: str):
        try:
            self.process_batch(batch_id)
        except RetryError as e:
            # handle tenacity RetryError explicitly because it breaks celery autoretry
            logger.error("RetryError", exc=e)
            raise self.retry()

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        logger.error("on_failure", task_id=task_id, args=args, kwargs=kwargs)
        self.client.refresh_token()
        batch_id = kwargs["batch_id"]
        self.client.update_batch_processing_status(
            batch_id, ProcesingStatus.FAILED, str(exc)
        )


app.register_task(RunBatchPrediction())
