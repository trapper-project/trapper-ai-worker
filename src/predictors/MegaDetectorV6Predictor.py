from predictors.base import AIPredictor

import numpy as np
from ultralytics import YOLO

from pathlib import Path


class MegaDetectorV6Predictor(AIPredictor):
    """
    MegaDetector V6 object detection based on YOLOv9 (MDV6b-yolov9c)

    This class is implementing Detector class from official MDv6 repository
    https://github.com/microsoft/CameraTraps/blob/main/PytorchWildlife/models/detection/ultralytics_based/megadetectorv6.py#L8
    """

    MODEL_FILE_EXTENSION = ".pt"
    CONFIDENCE_THRESHOLD: float = 0.4 # TODO: need to be verified with MD creators
    IOU_THRESHOLD: float = 0.7
    AGNOSTIC_NMS: bool = True

    def __init__(self, weights_path: Path, image_size: int):
        self.model = YOLO(model=weights_path)
        self.image_size = image_size

    def predict(self, image_matrix: np.array) -> dict:
        """
        YOLO-based MegaDetector object detection model predict objects classes
        """

        all_boxes = []
        all_scores = []
        all_classes = []

        for result in self.model(
            image_matrix,
            imgsz=self.image_size,
            conf=self.CONFIDENCE_THRESHOLD,
            iou=self.IOU_THRESHOLD,
            agnostic_nms=self.AGNOSTIC_NMS,
        ):
            predictions = result.boxes.data.cpu().numpy()
            boxes = predictions[:, :4].copy()

            boxes[:, 0] = predictions[:, 1] / image_matrix.shape[0]
            boxes[:, 1] = predictions[:, 0] / image_matrix.shape[1]
            boxes[:, 2] = predictions[:, 3] / image_matrix.shape[0]
            boxes[:, 3] = predictions[:, 2] / image_matrix.shape[1]

            all_boxes.extend(boxes.tolist())
            all_scores.extend(predictions[:, 4].tolist())
            all_classes.extend(predictions[:, 5].tolist())

        return {"bboxes": all_boxes, "scores": all_scores, "classes": all_classes}
