#!/bin/bash

set -e

export IMAGE_NAME=registry.gitlab.com/oscf/trapper-ai-worker

COLOR_RED_BRIGHT='\033[1;31m'
COLOR_GREEN_BRIGHT='\033[1;32m'
COLOR_BLUE_BRIGHT='\033[1;34m'
NO_COLOR='\033[0m' # No Color

# Defaults
export TRAPPERAI_LOG_LEVEL=info
export DEV_MODE=0
export STAGING_SWITCH=""

export COMPOSE_PROJECT_NAME="trapper-ai-worker"

export DOCKERFILE=${DOCKERFILE:-docker/Dockerfile}

# Default Celery worker concurrency
export CONCURRENCY=1

CODE_DIR=$(pwd)

ACTIVE_BRANCH=main

# Global variables
fragments=()

# Utility functions

load_dotenv() {
  if [ -f .env ]; then
    print_info "Loading environment variables from .env file..."
    . .env
  else
    print_info "No .env file, skipping..."
  fi
}

print_err() {
  echo -e "${COLOR_RED_BRIGHT}[ERROR]${NO_COLOR} $1"
  exit 1
}
print_ok() {
  echo -e "${COLOR_GREEN_BRIGHT}[OK]${NO_COLOR} $1"
}
print_info() {
  echo -e "${COLOR_BLUE_BRIGHT}[INFO]${NO_COLOR} $1"
}

print_warn() {
  echo -e "${COLOR_RED_BRIGHT}[WARN]${NO_COLOR} $1"
}

# Deployment functions

build_images() {
  print_info "Building runtime worker image"
  docker build . -f "${DOCKERFILE}" -t "${IMAGE_NAME}:${IMAGE_TAG_WORKER}"
}

pull_images() {
  docker pull "${IMAGE_NAME}:${IMAGE_TAG_WORKER}"
  docker compose "${compose_files[@]}" pull
}

export_image_tags() {
  if [ "$BUILD_IMAGES" = 1 ]; then
    ACTIVE_BRANCH=local
  fi
  export IMAGE_TAG_WORKER=$ACTIVE_BRANCH
}

obtain_images() {
  if [ "$BUILD_IMAGES" = 1 ]; then
    print_info "BUILD_IMAGES flag enabled, building images"
    build_images
  else
    print_info "Images will be pulled from Docker registry"
    pull_images
  fi
}

configure_gpu_usage() {
  if [ "${USE_GPU}" = 1 ]; then
    print_info "Using GPU"
    fragments+=("gpu")
  else
    print_info "Not using GPU"
  fi
}

configure_volumes() {
  if [ "${MOUNT_VOLUMES}" = 1 ]; then
    if [ -z "$VOLUMES_DIR" ]; then
      VOLUMES_DIR="$(pwd)/volumes"
      print_warn "VOLUMES_DIR not set, defaulting to ${VOLUMES_DIR}"
    fi

    print_ok "Mounting volumes to ${VOLUMES_DIR}"
    export VOLUMES_DIR
    fragments+=("volumes")
  else
    print_warn "Using named volumes for storing data"
  fi
}

configure_dev_mode() {
  if [ "$DEV_MODE" = 1 ]; then
    fragments+=("dev")
    print_info "Enabling development mode"
    export DEV_MODE CODE_DIR
  fi
}

check_required_env_var() {
  local var=$1
  if [ -n "${!var}" ]; then
    print_ok "$var set"
  else
    print_err "Required environment variable $var not present, aborting"
  fi
}


check_required_env_variables() {
  required_vars=("TRAPPERAI_USERNAME" "TRAPPERAI_PASSWORD" "TRAPPERAI_URL" "TRAPPERAI_RABBIT_URL" "QUEUE_NAME")
  print_info "Checking basic configuration"
  for var in "${required_vars[@]}"; do
    check_required_env_var "$var"
    export "${var?}"
  done
}

compose_files=()
aggregate_compose_files() {
  for fragment in "${fragments[@]}"; do
    compose_files+=("-f" "docker/docker-compose.${fragment}.yml")
  done
}

prepare_config() {
  load_dotenv
  check_required_env_variables

  fragments+=("base")

  configure_volumes
  configure_gpu_usage
  configure_dev_mode

  aggregate_compose_files
}

start() {
  print_info "Compose files: ${compose_files[*]}"
  docker compose "${compose_files[@]}" up --remove-orphans
}

start_detached() {
  print_info "Compose files: ${compose_files[*]}"
  docker compose "${compose_files[@]}" up -d --remove-orphans
}

stop() {
  docker compose "${compose_files[@]}" down --remove-orphans
}

restart() {
  docker compose "${compose_files[@]}" restart
}

logs() {
  docker compose "${compose_files[@]}" logs -f
}

docker_shell() {
  print_info "Entering TrapperAI container"
  docker compose "${compose_files[@]}" run --rm worker-ai bash
}

main() {
  local cmd=$1
  prepare_config
  export_image_tags

  if [ "$cmd" = "start" ]; then
    obtain_images
    start_detached
  elif [ "$cmd" = "start-i" ]; then
    obtain_images
    start
  elif [ "$cmd" = "stop" ]; then
    stop
  elif [ "$cmd" = "logs" ]; then
    logs
  elif [ "$cmd" = "shell" ]; then
    docker_shell
  elif [ "$cmd" = "restart" ]; then
    restart
  else
    print_err "Invalid command: ${cmd}"
  fi
}

main "$1"
