from click import Path

from .Yolov5Predictor import Yolov5Predictor

from .MegaDetectorV5Predictor import MegaDetectorV5Predictor
from .MegaDetectorV6Predictor import MegaDetectorV6Predictor
from .Yolov8Predictor import (
    Yolov8Predictor,
)
from .DeepFaunePredictor import (
    DeepFauneDetectionPredictor,
    DeepFauneClassificationPredictor
)


class AIPredictorFactory:
    def create_predictor(
        self, predictor_class: str, model_file_path: Path, model_config: dict
    ):
        if predictor_class == "MegaDetectorV5Predictor":
            return MegaDetectorV5Predictor(
                model_file_path,
                use_cpu=False,
                probability_threshold=model_config.get("probability_threshold", 0.1),
                swap_bgr=model_config.get("swap_bgr", False),
            )
        elif predictor_class == "MegaDetectorV6Predictor":
            return MegaDetectorV6Predictor(
                model_file_path,
                image_size=model_config.get("image_size", 640),
            )
        elif predictor_class == "DeepFauneDetectionPredictor":
            return DeepFauneDetectionPredictor(
                model_file_path,
                # 640px was mentioned in the DeepFaune paper
                # image_size=model_config.get("image_size", 640),
                image_size=model_config.get("image_size", 960),
            )
        elif predictor_class == "DeepFauneClassificationPredictor":
            return DeepFauneClassificationPredictor(
                model_file_path,
                num_classes=model_config.get("num_classes", 30),
                crop_size=model_config.get("crop_size", 182),
            )
        elif predictor_class == "Yolov5Predictor":
            return Yolov5Predictor(
                model_file_path,
                image_size=model_config.get("image_size", 1000),
            )
        elif predictor_class == "Yolov8Predictor":
            return Yolov8Predictor(
                model_file_path,
                image_size=model_config.get("image_size", 1024)
            )
        elif predictor_class == "YoloRPredictor":
            from .YoloRPredictor import YoloRPredictor

            return YoloRPredictor(
                model_file_path,
                image_size=model_config.get("image_size", 640),
                swap_bgr=model_config.get("swap_bgr", True),
                config_name=model_config.get("config_name", "yolor_csp"),
                confidence_threshold=model_config.get("confidence_threshold", 0.25),
                iou_threshold=model_config.get("iou_threshold", 0.5),
            )
        elif predictor_class == "DarknetPredictor":
            from .DarknetPredictor import DarknetPredictor

            return DarknetPredictor(
                model_file_path,
                image_size=model_config.get("image_size", 640),
                swap_bgr=model_config.get("swap_bgr", False),
                config_name=model_config.get("config_name", "yolor_csp"),
                confidence_threshold=model_config.get("confidence_threshold", 0.25),
                iou_threshold=model_config.get("iou_threshold", 0.5),
            )
        else:
            raise NotImplementedError

    def get_model_file_extension(self):
        return ".pt"
