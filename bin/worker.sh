#!/bin/bash

set -e

export PYTHONPATH=$PYTHONPATH:/code/src
CONCURRENCY=${CONCURRENCY:-1}
echo "Starting worker with concurrency $CONCURRENCY"
exec ./bin/run.sh celery -A worker worker -l "$TRAPPERAI_LOG_LEVEL" -Q "$QUEUE_NAME" -c $CONCURRENCY --pool prefork

