#!/bin/bash

set -e

if [ "$DEV_MODE" = 1 ]; then
  exec watchmedo auto-restart --recursive -d . -p '*.py' -- python3 -m "$@"
else
  exec python3 -m "$@"
fi