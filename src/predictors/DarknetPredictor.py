import logging
from pathlib import Path
import numpy as np
import torch

from predictors.utils import (
    letterbox,
    non_max_suppression,
    scale_coords,
    truncate_float,
    truncate_float_array,
    yolo2xyxy,
)

from .base import AIPredictor

from .darknet.darknet import Darknet

log = logging.getLogger(__name__)


class DarknetPredictor(AIPredictor):
    MODEL_FILE_EXTENSION = ".pt"

    CONFIG_DIR = Path(__file__).parent / "darknet" / "cfg"

    CONFIG_PATH = Path(__file__).parent / "yolor_csp.cfg"
    STRIDE = 64

    CONFIDENCE_THRESHOLD: float = 0.25
    IOU_THRESHOLD: float = 0.5
    CONF_DIGITS = 3
    COORD_DIGITS = 4

    AGNOSTIC_NMS = False

    def __init__(
        self,
        weights_path: Path,
        image_size: int,
        use_cpu: bool = False,
        config_name: str = "yolor_csp",
        swap_bgr: bool = False,
        confidence_threshold: float = CONFIDENCE_THRESHOLD,
        iou_threshold: float = IOU_THRESHOLD,
    ):
        config_name = config_name.lower()
        if "/" in config_name or "\\" in config_name:
            raise ValueError("Invalid config name")
        config_file = f"{config_name}.cfg"
        config_path = self.CONFIG_DIR / config_file
        if not config_path.exists():
            raise ValueError(f"Config file {config_file} not found")
        self.model = Darknet(config_path, img_size=image_size)
        self.device = (
            torch.device("cuda:0")
            if torch.cuda.is_available() and not use_cpu
            else "cpu"
        )
        self.model.to(self.device)
        self.model.eval()
        self.model.load_state_dict(
            torch.load(weights_path, map_location=self.device)["model"]
        )
        self.image_size = image_size
        self.stride = self.STRIDE
        self.swap_bgr = swap_bgr
        self.confidence_threshold = confidence_threshold
        self.iou_threshold = iou_threshold

    def predict(self, image_matrix: np.array) -> dict:
        with torch.no_grad():
            img = self._preprocess_input_image(image_matrix)
            raw_results = self.model(img, verbose=False)

            prediction = raw_results[0].clone()

            bboxes = []
            scores = []
            classes = []

            # NMS
            log.info("Using confidence threshold of %s", self.confidence_threshold)
            pred = non_max_suppression(
                prediction=prediction,
                conf_thres=self.confidence_threshold,
                iou_thres=self.iou_threshold,
                agnostic=self.AGNOSTIC_NMS,
            )

            # format detections/bounding boxes
            gn = torch.tensor(image_matrix.shape)[
                [0, 1, 0, 1]
            ]  # normalization gain hwhw

            for det in pred:
                if not len(det):
                    continue
                # Rescale boxes from img_size to im0 size
                det[:, :4] = scale_coords(
                    img.shape[2:], det[:, :4], image_matrix.shape
                ).round()

                for *xyxy, conf, cls in reversed(det):
                    xyxy = (
                        (yolo2xyxy(torch.tensor(xyxy).view(1, 4)) / gn)
                        .view(-1)
                        .tolist()
                    )
                    conf = truncate_float(conf.tolist(), precision=self.CONF_DIGITS)

                    cls = int(cls.tolist()) + 1

                    bboxes.append(
                        truncate_float_array(xyxy, precision=self.COORD_DIGITS)
                    )
                    scores.append(conf)
                    classes.append(float(cls))

            return {"bboxes": bboxes, "scores": scores, "classes": classes}

    def _preprocess_input_image(self, img_original):
        # padded resize
        # JIT requires auto=False
        img = letterbox(
            img_original, new_shape=self.image_size, stride=self.stride, auto=True
        )[0]

        if self.swap_bgr:
            img = img[:, :, ::-1]

        img = img.transpose((2, 0, 1))
        img = np.ascontiguousarray(img)
        img = torch.from_numpy(img)
        img = img.to(self.device)
        img = img.float()
        img /= 255

        if (
            len(img.shape) == 3
        ):  # always true for now, TODO add inference using larger batch size
            img = torch.unsqueeze(img, 0)

        return img
