<p align="center">
  <img src="OSCF-TRAPPER-AI-logo.png">
</p>

<div align="center"> 
<font size="6"> AI worker(s) for object detection and species classification </font>
<br>
<hr> 
<img src="https://img.shields.io/badge/Python-3.12-blue"/>
<img src="https://img.shields.io/badge/CUDA-12.4-darkgreen"/>
<a href="https://demo.trapper-project.org/"><img src="https://img.shields.io/badge/Trapper-Demo-green" /></a>
<a href="https://trapper-project.readthedocs.io/en/latest/"><img src="https://img.shields.io/badge/Trapper-Documentation-yellow" /></a>
<a href="https://besjournals.onlinelibrary.wiley.com/doi/10.1111/2041-210X.12571"><img src="https://img.shields.io/badge/Trapper-Paper-blue.svg" /></a>
<a href="https://join.slack.com/t/trapperproject/shared_invite/zt-2f360a5pu-CzsIqJ6Y~iCa_dmGXVNB7A"><img src="https://img.shields.io/badge/Trapper-Slack-orange" /></a>
<a href="https://gitlab.com/trapper-project/trapper/-/blob/master/LICENSE"><img src="https://img.shields.io/badge/Licence-GPLv3-pink" /></a>
<br><br>
</div>

## Table of Contents

[Overview](https://gitlab.com/oscf/trapper-ai-worker#-overview) |
[Installation](https://gitlab.com/oscf/trapper-ai-worker#-installation) |
[Demo](https://gitlab.com/oscf/trapper-ai-worker#-demo) |
[Documentation](https://gitlab.com/oscf/trapper-ai-worker#-documentation) | 
[Who is using TRAPPER?](https://gitlab.com/oscf/trapper-ai-worker#-who-is-using-trapper) | 
[Funders and Partners](https://gitlab.com/oscf/trapper-ai-worker#-funders-and-partners) | 
[Support](https://gitlab.com/oscf/trapper-ai-worker#-support) | 
[License](https://gitlab.com/oscf/trapper-ai-worker#-license)

## 🐺 Overview

TrapperAI Worker is responsible for initializing and executing AI models. It can utilize all GPU instances on the host via the NVIDIA CUDA driver or, if a GPU is not available, the CPU.

By default, TrapperAI Worker supports:

- **MegaDetector v5a**, **MegaDetector v6** or **DeepFaune** for empty, human, vehicle or animal detection;
- **Trapper AI Species Classifier** (CISIM v2, [TrapperAI v02.2024](https://huggingface.co/OSCF/TrapperAI-v02.2024)) developed by OSCF, which classifies 20 European mammal species;
- **DeepFaune v1.2** for 30 European mammal species;
- **DeerAI** (developed by MELES).

TrapperAI Worker can be easily extended to support any PyTorch-based AI models for wildlife conservation, such as **DeepFaune**.

## 📥 Installation

In order to deploy TrapperAI Worker using docker you will need Docker and docker-compose installed on your system.

#### Configuration

You will need to create `.env` file containing configuration options for TrapperAI Worker

- `TRAPPERAI_USERNAME` required; username in TrapperAI for this runtime worker
- `TRAPPERAI_PASSWORD` required; password in TrapperAI for this runtime worker
- `TRAPPERAI_URL` URL to the main TrapperAI app
- `TRAPPERAI_RABBIT_URL` RabbitMQ URL for TrapperAI instance. By default it will be the same host as ./TrapperAI main instance, and port 8072
- `BUILD_IMAGES` optional; when set to `1` all docker images will be build locally, otherwise images will be pulled from docker registry.
  By default, images are pulled from docker registry. Make sure that docker is authorized to pull images from `oscf/trapper-ai-worker` registry,
  for example by logging in using `docker login registry.gitlab.com`
- `ACTIVE_BRANCH` optional; default: `master`; Branch to use for pulling images.
- `COMPOSE_PROJECT_NAME` prefix used eg. for docker-compose container names, defaults to `trapper-ai-worker`
- `IMAGE_NAME` defaults to `registry.gitlab.com/oscf/trapper-ai-worker`
- `TRAPPERAI_LOG_LEVEL` defaults to `info`
- `DEV_MODE` if set to `1` project directory will be mounted as a docker volume. Gunicorn and celery workers will be restarted on each code change.
- `MOUNT_VOLUMES` if set to `1` volumes will be mounted to local file system, otherwise mounted volumes will be used
- `VOLUMES_DIR` defaults to `./volumes` and is used as mount point for volumes when `MOUNT_VOLUMES` is enabled
- `USE_GPU` if set to 1, GPU will be used in AI worker

#### TrapperAI deployment commands

You can use `./trapperai_worker.sh` script to deploy entire TrapperAI stack

- `./trapperai_worker.sh start` start entire stack using `.env` config
- `./trapperai_worker.sh start-i` start "interactive", so stack will be stopped when you use ctrl+c
- `./trapperai_worker.sh logs` attach to container logs for all containers
- `./trapperai_worker.sh stop` stop running stack
- `./trapperai_worker.sh shell` starts new interactive bash session in the `web` container

## 🌐 Demo
[Trapper Expert ](https://demo.trapper-project.org) demo instance does not include **Trapper Citizen Science** or **Trapper AI** yet.

## 📝 Documentation

[TRAPPER](https://trapper-project.readthedocs.io) documentation.

If you are using TRAPPER or TrapperAI, please cite our work:

> Bubnicki, J.W., Churski, M. and Kuijper, D.P.J. (2016), trapper: an open source web-based application to manage camera trapping projects. Methods Ecol Evol, 7: 1209-1216. https://doi.org/10.1111/2041-210X.12571

> Choiński, M., Rogowski, M., Tynecki, P., Kuijper, D.P.J., Churski, M., Bubnicki, J.W. (2021). A First Step Towards Automated Species Recognition from Camera Trap Images of Mammals Using AI in a European Temperate Forest. In: Saeed, K., Dvorský, J. (eds) Computer Information Systems and Industrial Management. CISIM 2021. Lecture Notes in Computer Science(), vol 12883. Springer, Cham. https://doi.org/10.1007/978-3-030-84340-3_24

For more news about TRAPPER please visit the [Open Science Conservation Fund (OSCF) website](https://os-conservation.org) and [OSCF LinkedIn profile](https://www.linkedin.com/company/os-conservation/).

## 🏢 Who is using TRAPPER?
* Mammal Research Institute Polish Academy of Sciences;
* Karkonosze National Park;
* Swedish University of Agricultural Sciences;
* Svenska Jägareförbundet;
* Meles Wildbiologie;
* University of Freiburg Wildlife Ecology and Management;
* Bavarian Forest National Park;
* Georg-August-Universität Göttingen;
* KORA - Carnivore Ecology and Wildlife Management;
* and many more individual scientists and ecologies;

## 💲 Funders and Partners
<p align="center">
  <img src="TRAPPER-funds.png">
</p>
<p align="center">
  <img src="TRAPPER-partners.png">
</p>

## 🤝 Support

Feel free to add a new issue with a respective title and description on the [TRAPPER issue tracker](https://gitlab.com/trapper-project/trapper/-/issues). If you already found a solution to your problem, we would be happy to review your pull request.

If you prefer direct contact, please let us know: `contact@os-conservation.org`

We also have [TRAPPER Mailing List](https://groups.google.com/d/forum/trapper-project) and [TRAPPER Slack](https://join.slack.com/t/trapperproject/shared_invite/zt-2f360a5pu-CzsIqJ6Y~iCa_dmGXVNB7A).

## 📜 License

Read more in [TRAPPER License](https://gitlab.com/oscf/trapper-ai-worker/-/blob/main/LICENSE).
